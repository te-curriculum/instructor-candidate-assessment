# Module 3 Assessment Exercise

This assessment will help you validate your understanding of the module objectives using ASP.NET MVC. The following learning objectives are covered:

- Controllers
- Views & View logic using Razor
- Passing data from controllers to views
- Handling request data
- Dynamic-link creation
- Query parameter access
- Dependency Injection
- Data Access Objects

## Overview

The assessment uses a standard ASP.NET Core MVC Web Application template.

Whatever you complete and submit should not have any compile or run-time errors.

## Required Tools

The following tools/software are used to complete the assessment:

- Visual Studio 2017/2019 (support for .NET Core 2.1)
- SQL Server Express 201x

The script to set up the database is located under `scripts/setup.sql`.

## Instructions

1. Complete the `PuppySqlDao` class by implementing the `IPuppySqlDao` interface.
2. Create a controller action in `PuppiesController` that maps to `/Puppies`. This action must invoke the `GetPuppies()` method in the DAO. Pass the data returned from `GetPuppies()` to the `Index` view.
3. Modify the `Index` view so that it displays all the entries passed to it from the controller. Weights should have **lbs** after the numerical value and the `PaperTrained` value should show as **Yes** when `true` and **No** when `false`.
4. Create a controller action in `PuppiesController` to accept the request data generated from the form in the `Index` view. Invoke the `SavePuppy(Puppy)` method in the DAO to save a new entry in the database. After saving the data, redirect the client to `/Puppies`.
5. Create a controller action in `PuppiesController` that maps to `/Puppies/Detail/{id}`. This action must invoke the `GetPuppy(int)` method in the DAO. Pass the data that is returned to the the `Detail` view.
6. Modify the `Index` view by making each puppy's name a hyperlink that links to the controller action created in step 5.