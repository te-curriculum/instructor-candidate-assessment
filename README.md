# Instructor Interview Assessment

As part of the Tech Elevator instructor interview process, we would like for you to complete an assessment that we used to provide to our students. The solution you produce should demonstrate what would be considered a best practice while serving as a reference for a novice programmer.

The assessment is available as a .NET MVC or a Spring MVC application. You may choose either technology. Within the `dotnet` and `java` folders is a README with further instructions. 

The scope of the assessment covers the following concepts:

* Controllers and Views
* Razor/JSTL
* HTTP GET & HTTP POST
* Model binding
* Dependency Injection
* Data Access Objects (DAOs)

> Note for reference: If conducted in a classroom setting, we would limit the time students spend completing this assessment to two hours.

## Submit Your Work

Please follow these steps to work on the assessment and submit your work.

1. Provide a markdown document discussing any thoughts or assumptions you made when approaching the solution.
2. Create a [git bundle](http://schacon.github.io/git/git-bundle.html) of the repository using the command `git bundle create your-name-assessment.bundle --all`.
3. Send the bundle to `instructor-assessment@techelevator.com`.

Thank you for taking the time to complete this.