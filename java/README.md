# Module 3 Assessment Exercise

This assessment will help you validate your understanding of the module objectives using Spring MVC. The following learning objectives are covered:

- Controllers
- Views & View logic using JSTL
- Passing data from controllers to views
- Handling request data
- Dynamic-link creation
- Query parameter access
- Dependency Injection
- Data Access Objects

## Overview

The assessment uses Spring MVC as the Web Application framework. 

Whatever you complete and submit should not have any compile or run-time errors. 

## Required Tools

The following tools/software are used by students to complete the assessment:

- Eclipse or IntelliJ
- PostgreSQL

The script to set up the database is located under the `database` folder. Run `sh import-databases.sh` to build and populate the database.

## Instructions

1. Complete the `PuppyJdbcDao` class by implementing the `PuppyDao` interface.
2. Create a controller action in `PuppiesController` that maps to the root `/`. This action must invoke the `getAllPuppies()` method in the DAO. Pass the data returned from `getAllPuppies()` to the `puppyList.jsp` view.
3. Modify the `puppyList.jsp` view so that it displays all the entries passed to it from the controller. Weights should have **lbs** after the numerical value and the `PaperTrained` value should show as **Yes** when `true` and **No** when `false`.
4. Create a controller action in `PuppiesController` to accept the request data generated from the form in `puppyList.jsp`. Invoke the `savePuppy(Puppy)` method in the DAO to save a new entry in the database. After saving the data, redirect the client to `/`.
5. Create a controller action in `PuppiesController` that maps to `/puppy`. This action accepts an id as a query string parameter and then invokes the `getPuppy(int)` method in the DAO. Pass the data that is returned to the `puppyDetail.jsp` view.
6. Modify the `puppyList.jsp` view by making each puppy's name a hyperlink that links to the controller action created in step 5.